import App
import Vapor

var env = try Environment.detect()
// only for client testing
//var env = Environment.testing
try LoggingSystem.bootstrap(from: &env)
let app = Application(env)
defer { app.shutdown() }
try configure(app)
try app.run()
