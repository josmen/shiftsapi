//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 4/2/23.
//

import Foundation

extension Date {
    init?(_ year: Int, _ month: Int, _ day: Int) {
        guard let date = DateComponents(calendar: Calendar(identifier: .gregorian), year: year, month: month, day: day, hour: 12).date else {
            return nil
        }
        self = date
    }
}
