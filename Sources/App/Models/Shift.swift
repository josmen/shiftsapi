//
//  Shift.swift
//  
//
//  Created by Jose Antonio Mendoza on 1/2/23.
//

import Fluent
import Vapor

final class Shift: Model {
    static var schema: String = "shifts"
    
    struct FieldKeys {
        struct v1 {
            static var name: FieldKey { "name" }
            static var start: FieldKey { "start" }
            static var end: FieldKey { "end" }
            static var saturation: FieldKey { "saturation" }
            static var shiftsGroup: FieldKey { "group_id" }
        }
    }
    
    @ID var id: UUID?
    @Field(key: FieldKeys.v1.name) var name: String
    @Field(key: FieldKeys.v1.start) var start: String
    @Field(key: FieldKeys.v1.end) var end: String
    @Field(key: FieldKeys.v1.saturation) var saturation: Double
    @Parent(key: FieldKeys.v1.shiftsGroup) var shiftsGroup: ShiftsGroup
    
    init() { }
    
    init(id: UUID? = nil,
         name: String,
         start: String,
         end: String,
         saturation: Double,
         shiftsGroupID: ShiftsGroup.IDValue)
    {
        self.id = id
        self.name = name
        self.start = start
        self.end = end
        self.saturation = saturation
        self.$shiftsGroup.id = shiftsGroupID
    }
}

extension Shift: Content {}
