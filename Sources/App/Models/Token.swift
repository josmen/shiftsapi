//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 17/2/23.
//

import Fluent
import Vapor

final class Token: Model, Content {
    static var schema: String = "tokens"
    
    struct FieldKeys {
        struct v1 {
            static var value: FieldKey { "value" }
            static var user: FieldKey { "user_id" }
            static var createdAt: FieldKey { "created_at" }
        }
    }
    
    @ID var id: UUID?
    @Field(key: FieldKeys.v1.value) var value: String
    @Parent(key: FieldKeys.v1.user) var user: User
    @Timestamp(key: FieldKeys.v1.createdAt, on: .create) var createdAt: Date?
    
    init() { }
    
    init(id: UUID? = nil,
         value: String,
         userID: User.IDValue)
    {
        self.id = id
        self.value = value
        self.$user.id = userID
    }
}

extension Token {
    static func generate(for user: User) throws -> Token {
        let random = [UInt8].random(count: 16).base64
        return try Token(value: random, userID: user.requireID())
    }
}

extension Token: ModelTokenAuthenticatable {
    static let valueKey = \Token.$value
    static let userKey = \Token.$user
    
    typealias User = App.User
    
    var isValid: Bool {
        guard let createdAt else { return false }
        let expirationDate = Calendar.current.date(byAdding: .day, value: 5, to: createdAt)
        let currentDate = Date()
        return expirationDate ?? Date() > currentDate
    }
}
