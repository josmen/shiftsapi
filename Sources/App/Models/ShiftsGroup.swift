//
//  ShiftsGroup.swift
//  
//
//  Created by Jose Antonio Mendoza on 1/2/23.
//

import Fluent
import Vapor

final class ShiftsGroup: Model {
    static var schema: String = "shifts_groups"
    
    struct FieldKeys {
        struct v1 {
            static var validFrom: FieldKey { "valid_from" }
            static var category: FieldKey { "category" }
            static var location: FieldKey { "location" }
        }
    }
    
    @ID var id: UUID?
    @Field(key: FieldKeys.v1.validFrom) var validFrom: Date
    @Field(key: FieldKeys.v1.category) var category: String
    @Field(key: FieldKeys.v1.location) var location: String
    @Children(for: \Shift.$shiftsGroup) var shifts: [Shift]
    
    init() { }
    
    init(id: UUID? = nil, validFrom: Date, category: String, location: String) {
        self.id = id
        self.validFrom = validFrom
        self.category = category
        self.location = location
    }
}

extension ShiftsGroup: Content {}
