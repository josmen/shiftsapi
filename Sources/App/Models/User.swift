//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 17/2/23.
//

import Fluent
import Vapor

final class User: Model, Content {
    static var schema: String = "users"
    
    struct FieldKeys {
        struct v1 {
            static var name: FieldKey { "name" }
            static var username: FieldKey { "username" }
            static var password: FieldKey { "password" }
        }
    }
    
    @ID var id: UUID?
    @Field(key: FieldKeys.v1.name) var name: String
    @Field(key: FieldKeys.v1.username) var username: String
    @Field(key: FieldKeys.v1.password) var password: String
    
    init() { }
    
    init(id: UUID? = nil,
         name: String,
         username: String,
         password: String)
    {
        self.id = id
        self.name = name
        self.username = username
        self.password = password
    }
    
    final class Public: Content {
        var id: UUID?
        var name: String
        var username: String
        
        init(id: UUID?, name: String, username: String) {
            self.id = id
            self.name = name
            self.username = username
        }
    }
}

extension User {
    func convertToPublic() -> User.Public {
        return User.Public(id: id, name: name, username: username)
    }
}

extension Collection where Element: User {
    func convertToPublic() -> [User.Public] {
        return self.map { $0.convertToPublic() }
    }
}

extension EventLoopFuture where Value == Array<User> {
    func convertToPublic() -> EventLoopFuture<[User.Public]> {
        return self.map { $0.convertToPublic() }
    }
}

extension User: ModelAuthenticatable {
    static let usernameKey = \User.$username
    static var passwordHashKey = \User.$password
    
    func verify(password: String) throws -> Bool {
        try Bcrypt.verify(password, created: self.password)
    }
}
