import Fluent
import Vapor

func routes(_ app: Application) throws {
    let shiftsController = ShiftsController()
    try app.register(collection: shiftsController)
    
    let shiftsGroupsController = ShiftsGroupsController()
    try app.register(collection: shiftsGroupsController)
    
    let usersController = UsersController()
    try app.register(collection: usersController)
}
