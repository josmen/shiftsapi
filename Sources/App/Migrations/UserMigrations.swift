//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 17/2/23.
//

import Fluent
import Vapor

enum UserMigrations {
    struct v1: AsyncMigration {
        func prepare(on database: Database) async throws {
            try await database.schema(User.schema)
                .id()
                .field(User.FieldKeys.v1.name, .string, .required)
                .field(User.FieldKeys.v1.username, .string, .required)
                .field(User.FieldKeys.v1.password, .string, .required)
                .unique(on: User.FieldKeys.v1.username)
                .create()
        }
        
        func revert(on database: Database) async throws {
            try await database.schema(User.schema).delete()
        }
    }
    
    struct createAdmin: AsyncMigration {
        func prepare(on database: Database) async throws {
            let name = "Admin", username = "admin", password = "password"
            let user = User(name: name, username: username, password: try Bcrypt.hash(password))
            try await user.create(on: database)
        }
        
        func revert(on database: Database) async throws {
            try await User.query(on: database).filter(\.$username == "admin").delete()
        }
    }
}
