//
//  CreateShift.swift
//  
//
//  Created by Jose Antonio Mendoza on 1/2/23.
//

import Fluent
import Foundation

enum ShiftMigrations {
    struct v1: AsyncMigration {
        func prepare(on database: Database) async throws {
            try await database.schema(Shift.schema)
                .id()
                .field(Shift.FieldKeys.v1.name, .string, .required)
                .field(Shift.FieldKeys.v1.start, .string, .required)
                .field(Shift.FieldKeys.v1.end, .string, .required)
                .field(Shift.FieldKeys.v1.saturation, .double, .required)
                .field(Shift.FieldKeys.v1.shiftsGroup, .uuid)
                .foreignKey(Shift.FieldKeys.v1.shiftsGroup,
                            references: ShiftsGroup.schema, .id,
                            onDelete: .cascade)
                .create()
        }
        
        func revert(on database: Database) async throws {
            try await database.schema(Shift.schema).delete()
        }
    }
    
    struct seed: AsyncMigration {
        func prepare(on database: Database) async throws {
            let groups = (1...4).map { index in
                let categories = ["maquinista", "usi"]
                let locations = ["benidorm", "campello", "denia"]

                return ShiftsGroup(
                    validFrom: Date().addingTimeInterval(-Double.random(in: 0...(86400 * 60))),
                    category: categories.randomElement()!,
                    location: locations.randomElement()!)
            }
            try await groups.create(on: database)
            
            try await (1...10).map { index in
                Shift(name: "Shift #\(index)",
                      start: "\(index + 3 < 10 ? index + 3 : index):00",
                      end: "\(index + 10):00",
                      saturation: Double.random(in: 50...90),
                      shiftsGroupID: groups[Int.random(in: 0..<groups.count)].id!)
            }
            .create(on: database)
        }
        
        func revert(on database: Database) async throws {
            try await Shift.query(on: database).delete()
            try await ShiftsGroup.query(on: database).delete()
        }
    }
}
