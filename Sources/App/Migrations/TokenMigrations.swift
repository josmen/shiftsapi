//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 17/2/23.
//

import Fluent

enum TokenMigrations {
    struct v1: Migration {
        func prepare(on database: Database) -> EventLoopFuture<Void> {
            database.schema(Token.schema)
                .id()
                .field(Token.FieldKeys.v1.value, .string, .required)
                .field(Token.FieldKeys.v1.user, .uuid, .required, .references(User.schema, .id, onDelete: .cascade))
                .field(Token.FieldKeys.v1.createdAt, .date, .required)
                .create()
        }
        
        func revert(on database: Database) -> EventLoopFuture<Void> {
            database.schema(Token.schema).delete()
        }
    }
}
