//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 17/2/23.
//

import Fluent

enum ShiftsGroupMigrations {
    struct v1: AsyncMigration {
        func prepare(on database: Database) async throws {
            try await database.schema(ShiftsGroup.schema)
                .id()
                .field(ShiftsGroup.FieldKeys.v1.validFrom, .date, .required)
                .field(ShiftsGroup.FieldKeys.v1.category, .string, .required)
                .field(ShiftsGroup.FieldKeys.v1.location, .string, .required)
                .create()
        }
        
        func revert(on database: Database) async throws {
            try await database.schema(ShiftsGroup.schema).delete()
        }
    }
}
