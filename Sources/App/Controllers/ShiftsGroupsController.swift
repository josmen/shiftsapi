//
//  ShiftsGroupsController.swift
//  
//
//  Created by Jose Antonio Mendoza on 1/2/23.
//

import Fluent
import Vapor

struct ShiftsGroupsController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let shiftsGroupsRoute = routes.grouped("api", "groups")
        
        // MARK: - Public routes
        shiftsGroupsRoute.get(use: getAllHandler)
        shiftsGroupsRoute.get(":groupID", use: getHandler)
        shiftsGroupsRoute.get(":groupID", "shifts", use: getShiftsHandler)
        shiftsGroupsRoute.get("sorted", use: sortedHandler)
        
        // MARK: - Protected routes
        let tokenAuthMiddleware = Token.authenticator()
        let guardAuthMiddleware = User.guardMiddleware()
        let tokenAuthGroup = shiftsGroupsRoute.grouped(
            tokenAuthMiddleware,
            guardAuthMiddleware
        )

        tokenAuthGroup.post(use: createHandler)
        tokenAuthGroup.put(":groupID", use: updateHandler)
        tokenAuthGroup.delete(":groupID", use: deleteHandler)
    }
    
    // GET /api/groups
    func getAllHandler(_ req: Request) async throws -> [ShiftsGroup] {
        let groups = try await ShiftsGroup.query(on: req.db)
            .with(\.$shifts)
            .all()
        
        return groups
    }
    
    // GET /api/groups/{groupID}
    func getHandler(_ req: Request) async throws -> ShiftsGroup {
        guard let group = try await ShiftsGroup.find(req.parameters.get("groupID"), on: req.db) else {
            throw Abort(.notFound)
        }
        try await group.$shifts.load(on: req.db)
        return group
    }
    
    // GET /api/groups/{groupID}/shifts
    func getShiftsHandler(_ req: Request) async throws -> [Shift] {
        guard let group = try await ShiftsGroup.find(req.parameters.get("groupID"), on: req.db) else {
            throw Abort(.notFound)
        }
        return try await group.$shifts.query(on: req.db).all()
    }
    
    // GET /api/groups/sorted
    func sortedHandler(_ req: Request) async throws -> [ShiftsGroup] {
        try await ShiftsGroup.query(on: req.db)
            .sort(\.$validFrom, .descending)
            .with(\.$shifts)
            .all()
    }
    
    // POST /api/groups
    func createHandler(_ req: Request) async throws -> ShiftsGroup {
        let shiftsGroup = try req.content.decode(ShiftsGroup.self)
        try await shiftsGroup.save(on: req.db)
        try await shiftsGroup.$shifts.load(on: req.db)
        return shiftsGroup
    }
    
    // PUT /api/groups/{groupID}
    func updateHandler(_ req: Request) async throws -> ShiftsGroup {
        let updatedShiftsGroup = try req.content.decode(ShiftsGroup.self)
        
        guard let shiftsGroup = try await ShiftsGroup.find(req.parameters.get("groupID"), on: req.db) else {
            throw Abort(.notFound)
        }
        
        shiftsGroup.validFrom = updatedShiftsGroup.validFrom
        shiftsGroup.category = updatedShiftsGroup.category
        shiftsGroup.location = updatedShiftsGroup.location
        
        try await shiftsGroup.save(on: req.db)
        try await shiftsGroup.$shifts.load(on: req.db)
        return shiftsGroup
    }
    
    // DELETE /api/groups/{groupID}
    func deleteHandler(_ req: Request) async throws -> HTTPStatus {
        guard let shiftsGroup = try await ShiftsGroup.find(req.parameters.get("groupID"), on: req.db) else {
            throw Abort(.notFound)
        }
        try await shiftsGroup.delete(on: req.db)
        return .ok
    }
}
