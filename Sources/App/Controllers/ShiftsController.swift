//
//  ShiftsController.swift
//  
//
//  Created by Jose Antonio Mendoza on 1/2/23.
//

import Fluent
import Vapor

struct ShiftsController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let shiftsRoutes = routes.grouped("api", "shifts")
        
        // MARK: - Public routes
        shiftsRoutes.get(use: getAllHandler)
        shiftsRoutes.get(":shiftID", use: getHandler)
//        shiftsRoutes.get("actual", use: getActualShifts)
        shiftsRoutes.get("actual", use: getActualShiftsByCategoryAndLocation)
        shiftsRoutes.get(":shiftID", "group", use: getGroupHandler)
        
        // MARK: - Protected routes
        let tokenAuthMiddleware = Token.authenticator()
        let guardAuthMiddleware = User.guardMiddleware()
        let tokenAuthGroup = shiftsRoutes.grouped(
            tokenAuthMiddleware,
            guardAuthMiddleware
        )
        
        tokenAuthGroup.post(use: createHandler)
        tokenAuthGroup.put(":shiftID", use: updateHandler)
        tokenAuthGroup.delete(":shiftID", use: deleteHandler)
    }
    
    // GET /api/shifts
    func getAllHandler(_ req: Request) async throws -> [Shift] {
        try await Shift.query(on: req.db).all()
    }
    
    // GET /api/shifts/{shiftID}
    func getHandler(_ req: Request) async throws -> Shift {
        guard let shift = try await Shift.find(req.parameters.get("shiftID"), on: req.db) else {
            throw Abort(.notFound)
        }
        return shift
    }
    
    // GET /api/shifts/actual?category=maquinista&location=benidorm
    func getActualShiftsByCategoryAndLocation(_ req: Request) async throws -> [Shift] {
        guard let category = req.query[String.self, at: "category"],
              let location = req.query[String.self, at: "location"] else {
            throw Abort(.badRequest)
        }
        
        guard let group = try await ShiftsGroup.query(on: req.db)
            .filter(\.$category == category)
            .filter(\.$location == location)
            .sort(\.$validFrom, .descending)
            .first()
        else {
            throw Abort(.notFound)
        }
        return try await group.$shifts.query(on: req.db).all()
    }
    
    // GET /api/shifts/{shiftID}/group
    func getGroupHandler(_ req: Request) async throws -> ShiftsGroup {
        guard let shift = try await Shift.find(req.parameters.get("shiftID"), on: req.db),
              let group = try await shift.$shiftsGroup.query(on: req.db).first()
        else {
            throw Abort(.notFound)
        }
        return group
    }
    
    // POST /api/shifts
    func createHandler(_ req: Request) async throws -> Shift {
        let data = try req.content.decode(CreateShiftData.self)
        let shift = Shift(
            name: data.name,
            start: data.start,
            end: data.end,
            saturation: data.saturation,
            shiftsGroupID: data.shiftsGroupID)
        try await shift.save(on: req.db)
        return shift
    }
    
    // PUT /api/shifts/{shiftID}
    func updateHandler(_ req: Request) async throws -> Shift {
        let updatedShift = try req.content.decode(CreateShiftData.self)
        guard let shift = try await Shift.find(req.parameters.get("shiftID"), on: req.db) else {
            throw Abort(.notFound)
        }
        
        shift.name = updatedShift.name
        shift.start = updatedShift.start
        shift.end = updatedShift.end
        shift.saturation = updatedShift.saturation
        shift.$shiftsGroup.id = updatedShift.shiftsGroupID
        
        try await shift.save(on: req.db)
        return shift
    }
    
    // DELETE /api/shifts/{shiftID}
    func deleteHandler(_ req: Request) async throws -> HTTPStatus {
        guard let shift = try await Shift.find(req.parameters.get("shiftID"), on: req.db) else {
            throw Abort(.notFound)
        }
        try await shift.delete(on: req.db)
        return .ok
    }
}

struct CreateShiftData: Content {
    let name: String
    let start: String
    let end: String
    let saturation: Double
    let shiftsGroupID: UUID
}
