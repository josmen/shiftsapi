import Vapor

struct UsersController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let usersRoutes = routes.grouped("api", "users")
        
        // MARK: - Public routes
        usersRoutes.get(use: getAllHandler)
        usersRoutes.get(":userID", use: getHandler)
        
        let basicAuthMiddleware = User.authenticator()
        let basicAuthGroup = usersRoutes.grouped(basicAuthMiddleware)
        
        basicAuthGroup.post("login", use: loginHandler)
        
        // MARK: - Protected routes
        let tokenAuthMiddleware = Token.authenticator()
        let guardAuthMiddleware = User.guardMiddleware()
        let tokenAuthGroup = usersRoutes.grouped(
            tokenAuthMiddleware,
            guardAuthMiddleware
        )
        
        tokenAuthGroup.post(use: createHandler)
        tokenAuthGroup.put(":userID", use: updateHandler)
        tokenAuthGroup.delete(":userID", use: deleteHandler)
    }
    
    // GET /api/users
    func getAllHandler(_ req: Request) async throws -> [User.Public] {
        try await User.query(on: req.db).all().convertToPublic()
    }
    
    // GET /api/users/{userID}
    func getHandler(_ req: Request) async throws -> User.Public {
        guard let user = try await User.find(req.parameters.get("userID"), on: req.db) else {
            throw Abort(.notFound)
        }
        return user.convertToPublic()
    }
    
    // POST /api/users/login
    func loginHandler(_ req: Request) async throws -> Token {
        let user = try req.auth.require(User.self)
        let tokens = try await Token.query(on: req.db).all()

        let token = tokens.first(where: { token in
            token.$user.id == user.id!
        })
        if let token {
            // the user already has an old token
            if token.isValid {
                return token
            } else {
                try await token.delete(on: req.db)
            }
        }
        let newToken = try Token.generate(for: user)
        try await newToken.save(on: req.db)
        return newToken
    }
    
    // POST /api/users
    func createHandler(_ req: Request) async throws -> User.Public {
        let data = try req.content.decode(User.self)
        data.password = try Bcrypt.hash(data.password)
        let user = User(
            name: data.name,
            username: data.username,
            password: data.password)
        try await user.save(on: req.db)
        return user.convertToPublic()
    }
    
    // PUT /api/users/{userID}
    func updateHandler(_ req: Request) async throws -> User.Public {
        let updatedUser = try req.content.decode(User.self)
        updatedUser.password = try Bcrypt.hash(updatedUser.password)
        
        guard let user = try await User.find(req.parameters.get("userID"), on: req.db) else {
            throw Abort(.notFound)
        }
        
        user.name = updatedUser.name
        user.username = updatedUser.username
        user.password = updatedUser.password
        
        try await user.save(on: req.db)
        
        return user.convertToPublic()
    }
    
    // DELETE /api/users/{userID}
    func deleteHandler(_ req: Request) async throws -> HTTPStatus {
        guard let user = try await User.find(req.parameters.get("userID"), on: req.db) else {
            throw Abort(.notFound)
        }
        try await user.delete(on: req.db)
        return .ok
    }
}
