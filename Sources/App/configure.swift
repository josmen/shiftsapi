import Fluent
import FluentPostgresDriver
import Vapor

// configures your application
public func configure(_ app: Application) throws {
    app.databases.use(.postgres(
        hostname: Environment.get("DATABASE_HOST") ?? "localhost",
        port: Environment.get(app.environment == .testing ? "DATABASE_PORT_TEST" : "DATABASE_PORT").flatMap(Int.init(_:)) ?? 5432,
        username: Environment.get("DATABASE_USERNAME") ?? "vapor_username",
        password: Environment.get("DATABASE_PASSWORD") ?? "vapor_password",
        database: Environment.get(app.environment == .testing ? "DATABASE_NAME_TEST" : "DATABASE_NAME") ?? "vapor_database"
    ), as: .psql)

    // migrations
    app.migrations.add(ShiftsGroupMigrations.v1())
    app.migrations.add(ShiftMigrations.v1())
//    app.migrations.add(ShiftMigrations.seed())
    app.migrations.add(UserMigrations.v1())
    app.migrations.add(UserMigrations.createAdmin())
    app.migrations.add(TokenMigrations.v1())
    
    app.logger.logLevel = .debug
    
    try app.autoMigrate().wait()
    
    // register routes
    try routes(app)
}
