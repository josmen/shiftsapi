//
//  ShiftsTests.swift
//  
//
//  Created by Jose Antonio Mendoza on 4/2/23.
//

@testable import App
import XCTVapor

final class ShiftsTests: XCTestCase {
    let shiftName = "2"
    let shiftStart = "05:30"
    let shiftEnd = "13:30"
    let shiftSaturation = 90.8
    
    let shiftsPath = "/api/shifts/"
    var app: Application!
    
    override func setUp() {
        app = try! Application.testable()
    }
    
    override func tearDown() {
        app.shutdown()
    }
    
    func test_get_with_no_query_returns_404() throws {
        try app.test(.GET, "", afterResponse: { res in
            XCTAssertEqual(res.status, .notFound)
        })
    }
    
    func test_retrieving_shifts() throws {
        _ = try Shift.create(
            name: shiftName,
            start: shiftStart,
            end: shiftEnd,
            saturation: shiftSaturation,
            on: app.db)
        _ = try Shift.create(on: app.db)
        
        try app.test(.GET, shiftsPath, afterResponse: { response in
            XCTAssertEqual(response.status, .ok)
            let shifts = try response.content.decode([Shift].self)
            XCTAssertEqual(shifts.count, 2)
            XCTAssertEqual(shifts[0].name, shiftName)
            XCTAssertEqual(shifts[0].start, shiftStart)
            XCTAssertEqual(shifts[0].end, shiftEnd)
            XCTAssertEqual(shifts[0].saturation, shiftSaturation)
        })
    }
    
    func test_retrieving_a_single_shift() throws {
        let shift = try Shift.create(
            name: shiftName,
            start: shiftStart,
            end: shiftEnd,
            saturation: shiftSaturation,
            on: app.db)
        _ = try Shift.create(on: app.db)
        
        try app.test(.GET, "\(shiftsPath)\(shift.id!)", afterResponse: { response in
            XCTAssertEqual(response.status, .ok)
            let shift = try response.content.decode(Shift.self)
            XCTAssertEqual(shift.name, shiftName)
            XCTAssertEqual(shift.start, shiftStart)
            XCTAssertEqual(shift.end, shiftEnd)
            XCTAssertEqual(shift.saturation, shiftSaturation)
        })
    }
    
    func test_getting_the_group_of_a_shift() throws {
        let shift = try Shift.create(
            name: shiftName,
            start: shiftStart,
            end: shiftEnd,
            saturation: shiftSaturation,
            on: app.db)
        
        try app.test(.GET, "\(shiftsPath)\(shift.id!)/group", afterResponse: { response in
            XCTAssertEqual(response.status, .ok)
            
            let retrievedGroup = try response.content.decode(ShiftsGroup.self)
            XCTAssertNotNil(retrievedGroup.id)
            XCTAssertEqual(retrievedGroup.category, "maquinista")
            XCTAssertEqual(retrievedGroup.location, "benidorm")
        })
    }
    
    func test_saving_a_shift() throws {
        let user = try User.create(on: app.db)
        let group = try ShiftsGroup.create(on: app.db)
        let createShiftData = CreateShiftData(
            name: shiftName,
            start: shiftStart,
            end: shiftEnd,
            saturation: shiftSaturation,
            shiftsGroupID: group.id!)
        
        try app.test(.POST,
                     shiftsPath,
                     loggedInUser: user,
                     beforeRequest: { req in
                        try req.content.encode(createShiftData)
                    }, afterResponse: { response in
                        let receivedShift = try response.content.decode(Shift.self)
                        XCTAssertEqual(receivedShift.name, shiftName)
                        XCTAssertEqual(receivedShift.start, shiftStart)
                        XCTAssertEqual(receivedShift.end, shiftEnd)
                        XCTAssertEqual(receivedShift.saturation, shiftSaturation)
                        XCTAssertNotNil(receivedShift.id)
                        
                        try app.test(.GET, shiftsPath, afterResponse: { response in
                            XCTAssertEqual(response.status, .ok)
                            
                            let shifts = try response.content.decode([Shift].self)
                            XCTAssertEqual(shifts.count, 1)
                            XCTAssertEqual(shifts[0].name, shiftName)
                            XCTAssertEqual(shifts[0].start, shiftStart)
                            XCTAssertEqual(shifts[0].end, shiftEnd)
                            XCTAssertEqual(shifts[0].saturation, shiftSaturation)
                        })
                    }
        )
    }
    
    func test_updating_a_shift() throws {
        let user = try User.create(on: app.db)
        let shift = try Shift.create(
            name: shiftName,
            start: shiftStart,
            end: shiftEnd,
            saturation: shiftSaturation,
            on: app.db)
        let newGroup = try ShiftsGroup.create(on: app.db)
        let newShiftName = "8"
        let updatedShift = CreateShiftData(
            name: newShiftName,
            start: shiftStart,
            end: shiftEnd,
            saturation: shiftSaturation,
            shiftsGroupID: newGroup.id!)
        
        try app.test(.PUT, "\(shiftsPath)\(shift.id!)", loggedInUser: user, beforeRequest: { req in
            try req.content.encode(updatedShift)
        })
        
        try app.test(.GET, "\(shiftsPath)\(shift.id!)", afterResponse: { response in
            let retrievedShift = try response.content.decode(Shift.self)
            XCTAssertEqual(retrievedShift.name, newShiftName)
            XCTAssertEqual(retrievedShift.$shiftsGroup.id, newGroup.id)
        })
    }
    
    func test_deleting_a_shift() throws {
        let user = try User.create(on: app.db)
        let shift = try Shift.create(on: app.db)
        
        try app.test(.GET, shiftsPath, afterResponse: { response in
            let shifts = try response.content.decode([Shift].self)
            XCTAssertEqual(shifts.count, 1)
        })
        
        try app.test(.DELETE, "\(shiftsPath)\(shift.id!)", loggedInUser: user)
        
        try app.test(.GET, shiftsPath, afterResponse: { response in
            let shifts = try response.content.decode([Shift].self)
            XCTAssertEqual(shifts.count, 0)
        })
    }
}
