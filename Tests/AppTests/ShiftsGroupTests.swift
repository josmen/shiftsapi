//
//  ShiftsGroupTests.swift
//  
//
//  Created by Jose Antonio Mendoza on 3/2/23.
//

@testable import App
import XCTVapor

final class ShiftsGroupTests: XCTestCase {
    let validFrom = Date(2023, 1, 1)!
    let category = "tecnico"
    let location = "alicante"
    
    let groupsPath = "/api/groups/"
    var app: Application!
    
    override func setUpWithError() throws {
        app = try Application.testable()
    }
    
    override func tearDownWithError() throws {
        app.shutdown()
    }
    
    func test_retrieving_groups() throws {
        let _ = try ShiftsGroup.create(on: app.db)
        
        try app.test(.GET, groupsPath, afterResponse: { response in
            XCTAssertEqual(response.status, .ok)
            
            let groups = try response.content.decode([ShiftsGroup].self)
            XCTAssertEqual(groups.count, 1)
        })
    }
    
    func test_retrieving_a_single_group() throws {
        let group = try ShiftsGroup.create(
            validFrom: validFrom,
            category: category,
            location: location,
            on: app.db)
        
        try app.test(.GET, "\(groupsPath)\(group.id!)", afterResponse: { response in
            let receivedGroup = try response.content.decode(ShiftsGroup.self)
            
            XCTAssertEqual(receivedGroup.category, category)
            XCTAssertEqual(receivedGroup.location, location)
            XCTAssertEqual(receivedGroup.id, group.id)
        })
    }
    
    func test_retrieving_shifts_from_a_group() throws {
        let group = try ShiftsGroup.create(on: app.db)
        
        let shiftName = "2"
        let shiftStart = "05:27"
        let shiftEnd = "13:37"
        let shiftSaturation = 62.7
        
        let shiftTwo = try Shift.create(
            name: shiftName,
            start: shiftStart,
            end: shiftEnd,
            saturation: shiftSaturation,
            shiftsGroup: group,
            on: app.db)
        
        _ = try Shift.create(
            name: "3",
            start: "06:27",
            end: "14:37",
            saturation: 73.8,
            shiftsGroup: group,
            on: app.db)
        
        try app.test(.GET, "\(groupsPath)\(group.id!)/shifts", afterResponse: { response in
            let shifts = try response.content.decode([Shift].self)
            
            XCTAssertEqual(shifts.count, 2)
            XCTAssertEqual(shifts[0].id, shiftTwo.id)
            XCTAssertEqual(shifts[0].name, shiftName)
            XCTAssertEqual(shifts[0].start, shiftStart)
            XCTAssertEqual(shifts[0].end, shiftEnd)
            XCTAssertEqual(shifts[0].saturation, shiftSaturation)
        })
    }
    
    func test_saving_a_group() throws {
        let user = try User.create(on: app.db)
        let group = ShiftsGroup(
            validFrom: validFrom,
            category: category,
            location: location)
        
        try app.test(.POST, groupsPath, loggedInUser: user, beforeRequest: { req in
            try req.content.encode(group)
        }, afterResponse: { response in
            let receivedGroup = try response.content.decode(ShiftsGroup.self)
            
            XCTAssertEqual(receivedGroup.validFrom, validFrom)
            XCTAssertEqual(receivedGroup.category, category)
            XCTAssertEqual(receivedGroup.location, location)
            XCTAssertNotNil(receivedGroup.id)
            
            try app.test(.GET, groupsPath, afterResponse: { secondResponse in
                let groups = try secondResponse.content.decode([ShiftsGroup].self)
                
                XCTAssertEqual(groups.count, 1)
                XCTAssertEqual(groups[0].category, category)
                XCTAssertEqual(groups[0].location, location)
                XCTAssertEqual(groups[0].id, receivedGroup.id)
            })
        })
    }
    
    func test_updating_a_group() throws {
        let user = try User.create(on: app.db)
        let group = try ShiftsGroup.create(on: app.db)
        let updatedGroup = ShiftsGroup(
            validFrom: validFrom,
            category: category,
            location: location)
        
        try app.test(.PUT, "\(groupsPath)\(group.id!)", loggedInUser: user, beforeRequest: { req in
            try req.content.encode(updatedGroup)
        })
        
        try app.test(.GET, "\(groupsPath)\(group.id!)", afterResponse: { response in
            let retrievedGroup = try response.content.decode(ShiftsGroup.self)
            XCTAssertEqual(retrievedGroup.category, category)
            XCTAssertEqual(retrievedGroup.location, location)
        })
    }
    
    func test_deleting_a_group() throws {
        let user = try User.create(on: app.db)
        let group = try ShiftsGroup.create(on: app.db)
        
        try app.test(.DELETE, "\(groupsPath)\(group.id!)", loggedInUser: user, beforeRequest: { response in
            try app.test(.GET, groupsPath, afterResponse: { getResponse in
                XCTAssertEqual(getResponse.status, .ok)
                let groups = try getResponse.content.decode([ShiftsGroup].self)
                XCTAssertEqual(groups.count, 1)
            })
        }, afterResponse: { secondResponse in
            try app.test(.GET, groupsPath, afterResponse: { getResponse in
                XCTAssertEqual(getResponse.status, .ok)
                let groups = try getResponse.content.decode([ShiftsGroup].self)
                XCTAssertEqual(groups.count, 0)
            })
        })
    }
}
