//
//  Models+Testable.swift
//  
//
//  Created by Jose Antonio Mendoza on 3/2/23.
//

@testable import App
import Fluent
import Vapor

extension User {
    static func create(
        name: String = "Luke",
        username: String? = nil,
        on database: Database
    ) throws -> User {
        let createUsername: String
        if let username {
            createUsername = username
        } else {
            createUsername = UUID().uuidString
        }
        
        let password = try Bcrypt.hash("password")
        
        let user = User(name: name,
                        username: createUsername,
                        password: password)
        try user.save(on: database).wait()
        return user
    }
}

extension ShiftsGroup {
    static func create(
        validFrom: Date = Date(2023, 1, 1)!,
        category: String = "maquinista",
        location: String = "benidorm",
        on database: Database
    ) throws -> ShiftsGroup {
        let shiftsGroup = ShiftsGroup(validFrom: validFrom,
                                      category: category,
                                      location: location)
        try shiftsGroup.save(on: database).wait()
        return shiftsGroup
    }
}

extension Shift {
    static func create(
        name: String = "1",
        start: String = "04:41",
        end: String = "10:41",
        saturation: Double = 58.5,
        shiftsGroup: ShiftsGroup? = nil,
        on database: Database
    ) throws -> Shift {
        var group = shiftsGroup
        
        if group == nil {
            group = try ShiftsGroup.create(on: database)
        }
        
        let shift = Shift(name: name,
                          start: start,
                          end: end,
                          saturation: saturation,
                          shiftsGroupID: group!.id!)
        try shift.save(on: database).wait()
        return shift
    }
}
