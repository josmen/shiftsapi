//
//  File.swift
//  
//
//  Created by Jose Antonio Mendoza on 18/2/23.
//

@testable import App
import XCTVapor

final class UserTests: XCTestCase {
    let usersName = "Alice"
    let usersUsername = "alicea"
    let usersURI = "/api/users/"
    var app: Application!
    
    override func setUpWithError() throws {
        app = try Application.testable()
    }
    
    override func tearDownWithError() throws {
        app.shutdown()
    }
    
    func test_retrieving_users() throws {
        let user = try User.create(name: usersName, username: usersUsername, on: app.db)
        _ = try User.create(on: app.db)
        
        try app.test(.GET, usersURI, afterResponse: { response in
            
            XCTAssertEqual(response.status, .ok)
            let users = try response.content.decode([User.Public].self)
            
            XCTAssertEqual(users.count, 3)
            XCTAssertEqual(users[1].name, usersName)
            XCTAssertEqual(users[1].username, usersUsername)
            XCTAssertEqual(users[1].id, user.id)
        })
    }
    
    func test_retrieving_a_single_user() throws {
        let user = try User.create(name: usersName, username: usersUsername, on: app.db)
        
        try app.test(.GET, "\(usersURI)\(user.id!)", afterResponse: { response in
            let receivedUser = try response.content.decode(User.Public.self)
            XCTAssertEqual(receivedUser.name, usersName)
            XCTAssertEqual(receivedUser.username, usersUsername)
            XCTAssertEqual(receivedUser.id, user.id)
        })
    }
    
    func test_saving_a_user() throws {
        let user = User(name: usersName, username: usersUsername, password: "password")
        
        try app.test(.POST, usersURI, loggedInRequest: true, beforeRequest: { req in
            try req.content.encode(user)
        }, afterResponse: { response in
            let receivedUser = try response.content.decode(User.Public.self)
            XCTAssertEqual(receivedUser.name, usersName)
            XCTAssertEqual(receivedUser.username, usersUsername)
            XCTAssertNotNil(receivedUser.id)
            
            try app.test(.GET, usersURI, afterResponse: { secondResponse in
                let users = try secondResponse.content.decode([User.Public].self)
                XCTAssertEqual(users.count, 2)
                XCTAssertEqual(users[1].name, usersName)
                XCTAssertEqual(users[1].username, usersUsername)
                XCTAssertEqual(users[1].id, receivedUser.id)
            })
        })
    }
    
    func test_updating_a_user() throws {
        let user = try User.create(name: usersName, username: usersUsername, on: app.db)
        let newName = "Paco"
        let newUsername = "Jones"
        let updatedUser = User(name: newName, username: newUsername, password: "password")
        
        try app.test(.PUT, "\(usersURI)\(user.id!)", loggedInRequest: true, beforeRequest: { req in
            try req.content.encode(updatedUser)
        })
        
        try app.test(.GET, "\(usersURI)\(user.id!)", afterResponse: { response in
            let receivedUser = try response.content.decode(User.Public.self)
            XCTAssertEqual(receivedUser.name, newName)
            XCTAssertEqual(receivedUser.username, newUsername)
            XCTAssertEqual(receivedUser.id, user.id)
        })
        
        // clean
        try app.test(.DELETE, "\(usersURI)\(user.id!)", loggedInRequest: true)
    }
    
    func test_deleting_a_user() throws {
        let user = try User.create(on: app.db)
        
        try app.test(.DELETE, "\(usersURI)\(user.id!)", loggedInRequest: true, beforeRequest: { response in
            try app.test(.GET, usersURI, afterResponse: { getResponse in
                let users = try getResponse.content.decode([User.Public].self)
                XCTAssertEqual(users.count, 2)
            })
        }, afterResponse: { secondResponse in
            try app.test(.GET, usersURI, afterResponse: { getResponse in
                let users = try getResponse.content.decode([User.Public].self)
                XCTAssertEqual(users.count, 1)
            })
        })
    }
}
